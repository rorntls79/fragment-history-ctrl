package com.mslee.fraghistory

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.FragmentManager
import com.mslee.fraghistory.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
	private val mainName = "First page"
	private val bd by lazy { ActivityMainBinding.inflate(layoutInflater) }


	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		setContentView(bd.root)

		init()

//		addNewPage("First page")
	}

	private fun init() {
		supportFragmentManager.beginTransaction()
			.replace(bd.content.id, FragPage().apply {
				arguments = Bundle().apply {
					putString("label", mainName)
				}
			})
			.commit()
	}

	fun addNewPage(name: String) {
		val label = makeStackLabel() + " >> $name"

		supportFragmentManager.beginTransaction()
			.add(bd.content.id, FragPage().apply {
				arguments = Bundle().apply {
					putString("label", label)
				}
			})
			.addToBackStack(name)
			.commit()
	}

	private fun makeStackLabel(): String {
		var rtn = mainName

		for( idx in 0 until supportFragmentManager.backStackEntryCount ) {
			rtn += " >> "
			Log.e("###", "ID = ${supportFragmentManager.getBackStackEntryAt(idx).id}")
			Log.e("###", "ID = ${supportFragmentManager.getBackStackEntryAt(idx).name}")
			rtn += supportFragmentManager.getBackStackEntryAt(idx).name
		}

		return rtn
	}

	fun backStackTo(name: String) {
		supportFragmentManager.popBackStack(name, FragmentManager.POP_BACK_STACK_INCLUSIVE)
	}
}