package com.mslee.fraghistory

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mslee.fraghistory.databinding.FragPageBinding
import kotlin.random.Random

class FragPage: Fragment() {
	private val bd by lazy { FragPageBinding.inflate(layoutInflater) }
	private val act by lazy { activity as MainActivity }


	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View {
		val bgColor = ColorDrawable()
		bgColor.color = Color.rgb(
			Random.nextInt(0,255),
			Random.nextInt(0,255),
			Random.nextInt(0,255)
		)
		bd.root.background = bgColor
		return bd.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		bd.tvHistory.text = arguments?.getString("label", "ERROR")?: "ERROR"

		bd.btnAdd.setOnClickListener {
			act.addNewPage(bd.etName.text.toString().ifEmpty { "${System.currentTimeMillis()}" })
		}

		bd.btnBack.setOnClickListener {
			act.backStackTo(bd.etName.text.toString())
		}
	}
}